﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Painting : MonoBehaviour
{
    public Renderer rend;
    public GameObject renk;
    public float location;
    Ray ray;
    RaycastHit hit;

    void Start()
    {
        rend = GetComponent<Renderer>();
        rend.enabled = true;
        renk = GameObject.Find("Circle");
    }
    ///TouchScreen
    void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
            if (hit.collider != null)
            {
                hit.collider.GetComponent<Renderer>().sharedMaterial = renk.GetComponent<Renderer>().material;
            }
        }
    }

    ///MouseClick
    private void OnMouseDown()
    {

        if (Input.GetMouseButtonDown(0))
        {
            rend.sharedMaterial = renk.GetComponent<Renderer>().material;
        }
    }
}
