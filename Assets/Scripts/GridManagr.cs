﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManagr : MonoBehaviour
{

    [SerializeField] private int rows = 3;
    [SerializeField] private int cols = 3;
    [SerializeField] private float tileSize = 1.6f;
    SpriteRenderer m_SpriteRenderer;
    float scaleX;
    float scaleY;
    float scale;
    void Start()
    {
        GridStart(cols, rows);
    }
    public void Left()
    {
        if (cols != 3)
        {
            Destroy(GameObject.Find("Square(Clone)(Clone)"));
            Kare(cols - 1, rows);
            ColDelete();
            cols--;
        }
    }
    public void Up()
    {
        if (rows != 3)
        {
            Destroy(GameObject.Find("Square(Clone)(Clone)"));
            Kare(cols, rows - 1);
            RowDelete();
            rows--;
        }
    }
    public void Down()
    {
        if (rows != 7)
        {
            Destroy(GameObject.Find("Square(Clone)(Clone)"));
            Kare(cols, rows + 1);
            RowAdd();
            rows++;
        }
    }
    public void Right()
    {
        if (cols != 6)
        {            
            Destroy(GameObject.Find("Square(Clone)(Clone)"));
            Kare(cols + 1, rows);
            ColonAdd();
            cols++;
        }
    }
    private void ColonAdd()
    {
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                float posX = col * tileSize - (cols) * tileSize / 2;
                float posY = -row * tileSize + (rows - 1) * tileSize / 2;
                float posZ = -1.0f;
                GameObject.Find("Cir" + col + row).transform.position = new Vector3(posX * scale, posY * scale, posZ);
            }
        }

        GameObject referenceTile = (GameObject)Instantiate(Resources.Load("Circle1"));
        for (int row = 0; row < rows; row++)
        {          
            GameObject tile = (GameObject)Instantiate(referenceTile, transform);
            m_SpriteRenderer = tile.GetComponent<SpriteRenderer>();
            float posX = cols * tileSize - (cols) * tileSize / 2;
            float posY = -row * tileSize + (rows - 1) * tileSize / 2;
            float posZ = -1.0f;
            tile.transform.position = new Vector3(posX * scale, posY * scale, posZ);
            tile.transform.gameObject.name = "Cir" + cols + row;
        }
        Destroy(referenceTile);
    }
    private void RowAdd()
    {
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                float posX = col * tileSize - (cols - 1) * tileSize / 2;
                float posY = -row * tileSize + (rows) * tileSize / 2;
                float posZ = -1.0f;
                GameObject.Find("Cir" + col + row).transform.position = new Vector3(posX * scale, posY * scale, posZ);
            }
        }
        GameObject referenceTile = (GameObject)Instantiate(Resources.Load("Circle1"));
        for (int col = 0; col < cols; col++)
        {
            GameObject tile = (GameObject)Instantiate(referenceTile, transform);
            m_SpriteRenderer = tile.GetComponent<SpriteRenderer>();
            float posX = col * tileSize - (cols - 1) * tileSize / 2;
            float posY = -rows * tileSize + (rows) * tileSize / 2;
            float posZ = -1.0f;
            tile.transform.position = new Vector3(posX * scale, posY * scale, posZ);
            tile.transform.gameObject.name = "Cir" + col + rows;
        }
        Destroy(referenceTile);
    }
    private void ColDelete()
    {
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                float posX = col * tileSize - (cols - 2) * tileSize / 2;
                float posY = -row * tileSize + (rows - 1) * tileSize / 2;
                float posZ = -1.0f;
                GameObject.Find("Cir" + col + row).transform.position = new Vector3(posX * scale, posY * scale, posZ);
            }
        }
        for (int row = 0; row < rows; row++)
        {
            Destroy(GameObject.Find("Cir" + (cols - 1) + row));
        }
    }
    private void RowDelete()
    {
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                float posX = col * tileSize - (cols - 1) * tileSize / 2;
                float posY = -row * tileSize + (rows - 2) * tileSize / 2;
                float posZ = -1.0f;
                GameObject.Find("Cir" + col + row).transform.position = new Vector3(posX * scale, posY * scale, posZ);
            }
        }
        for (int col = 0; col < cols; col++)
        {
            Destroy(GameObject.Find("Cir" + col + (rows - 1)));
        }
    }
    private void GridStart(int cols, int rows)
    {
        transform.position = new Vector2(0, 0);
        GameObject referenceTile = (GameObject)Instantiate(Resources.Load("Circle1"));
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                GameObject tile = (GameObject)Instantiate(referenceTile, transform);
                m_SpriteRenderer = tile.GetComponent<SpriteRenderer>();
                float posX = col * tileSize - (cols - 1) * tileSize / 2;
                float posY = -row * tileSize + (rows - 1) * tileSize / 2;
                float posZ = -1.0f;
                tile.transform.position = new Vector3(posX, posY, posZ);
                tile.transform.gameObject.name = "Cir" + col + row;
            }
        }
        Destroy(referenceTile);
        transform.position = new Vector2(0, 0);
        GameObject reft = (GameObject)Instantiate(Resources.Load("Square"));
        GameObject sqr = (GameObject)Instantiate(reft, transform);
        m_SpriteRenderer = sqr.GetComponent<SpriteRenderer>();
        m_SpriteRenderer.color = Color.white;
        sqr.transform.position = new Vector3(0, 0, 1);
        sqr.transform.localScale = new Vector2(cols * tileSize, rows * tileSize);
        Destroy(reft);
    }
    private void Kare(int cols, int rows)
    {
        scaleX = 3.0f / Convert.ToSingle(cols);
        scaleY = 3.0f / Convert.ToSingle(rows);
        if (scaleX > scaleY)
        {
            transform.localScale = new Vector3(scaleY, scaleY, 0.5f);
            scale = scaleY;
        }
        else if (scaleX < scaleY)
        {
            transform.localScale = new Vector3(scaleX, scaleX, 0.5f);
            scale = scaleX;
        }
        else
        {
            transform.localScale = new Vector3(scaleX, scaleY, 0.5f);
            scale = scaleY;
        }
        transform.position = new Vector2(0, 0);
        GameObject reft = (GameObject)Instantiate(Resources.Load("Square"));
        GameObject sqr = (GameObject)Instantiate(reft, transform);
        m_SpriteRenderer = sqr.GetComponent<SpriteRenderer>();
        m_SpriteRenderer.color = Color.white;
        sqr.transform.position = new Vector3(0, 0, 1);
        sqr.transform.localScale = new Vector2(cols * tileSize, rows * tileSize);
        Destroy(reft);
        Debug.Log("kare olusturuldu");
    }
}
