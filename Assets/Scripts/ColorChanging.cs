﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanging : MonoBehaviour
{
    public Material[] materials;
    public GameObject circ;
    public Renderer rend1;
    public Renderer rend2;

    public int index = 1;
    private int ind = 2;

    void Start()
    {
        rend1 = GetComponent<Renderer>();
        rend1.enabled = true;
        rend2 = circ.GetComponent<Renderer>();
        rend2.enabled = true;
    }
    ///TouchScreen
    void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
            if (hit.collider.transform.tag == "Color")
            {
                if (materials.Length == 0)
                {
                    return;
                }
                else
                {
                    index += 1;
                    ind += 1;
                    if (index == materials.Length + 1)
                    {
                        index = 1;
                    }
                    if (ind == materials.Length + 1)
                    {
                        ind = 1;
                    }
                    print(index);
                    hit.collider.GetComponent<Renderer>().sharedMaterial = materials[index - 1];
                    rend2.sharedMaterial = materials[ind - 1];
                }
            }
        }
    }
    ///MouseClick
    private void OnMouseDown()
    {
        if (materials.Length == 0)
        {
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            index += 1;
            ind += 1;
            if (index == materials.Length + 1)
            {
                index = 1;
            }
            if (ind == materials.Length + 1)
            {
                ind = 1;
            }
            print(index);
            rend1.sharedMaterial = materials[index - 1];
            rend2.sharedMaterial = materials[ind - 1];
        }
    }
}
